package Controller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

import Model.Record;
import Utility.StringUtility;
import View.AppData;

public class RecordManager 
{
	private File file;
	private String strName,strPath,strPermission;
	private RandomAccessFile raf;
	
	public static String validate(String strData, String strRegex, String strDefault)
	{
		strData = strData.toLowerCase();
		
		if(strData.matches(strRegex))
			return strData;
		else
			return strDefault;
	}
	
	private void setPath(String strPath)
	{
		this.strPath = validate(strPath, AppData.RegexFilePath, AppData.DefaultFilePath);
	}
	
	private void setName(String strName)
	{
		this.strName = validate(strName, AppData.RegexFileName, AppData.DefaultFileName);
	}
	
	private void setPermission(String strPermission)
	{
		this.strPermission = validate(strPath, AppData.RegexFilePermission, AppData.DefaultFilePermission);
	}
	
	public String getPath()
	{
		return strPath;
	}
	
	public String getName()
	{
		return strName;
	}
	
	public String getPermission()
	{
		return strPermission;
	}
	
	//Constructor
	public RecordManager(String strPath,
			String strName, String strPermission)
	{
		this.setPath(strPath);
		this.setName(strName);
		this.setPermission(strPermission);
		
		File folder = new File(this.strPath);
		if(!folder.exists())
			folder.mkdirs();
	}
	//create

	public boolean create(String strPath, String strName)
	{
		if(this.raf == null)
		{
			try
			{
				file = new File (strPath+strName);
				this.raf = new RandomAccessFile(this.file, "rw");
				return true;
			}
			catch(FileNotFoundException e)
			{
				e.printStackTrace();
			}
		}
		return false;
	}
	
	//open&close
	public boolean open(String strPath, String strName, boolean bAppend)
	{
		this.file = new File(this.strPath + this.strName);
		if(!bAppend)
			this.file.delete();
		try
		{
			this.raf = new RandomAccessFile(this.file, this.strPermission);
		}
		catch (FileNotFoundException e)
		{
			return false;
		}
		return true;
	}
	
	public boolean close()
	{
		try
		{
			this.raf.close();
		}
		catch (IOException e)
		{
			return false;
		}
		this.raf = null;
		this.file = null;
		
		return true;
	}
	
	//write & write(pos)
	public void write(Record r)
	{
		r.write(this.raf,r);
	}
	public void write( Record r, int index) throws IOException
	{
		setPointer(index);
		raf.writeInt(r.getStudentIdentificationNumber());
		raf.writeUTF(r.getFirstName());
		raf.writeUTF(r.getLastName());
		raf.writeInt(r.getCreditsSoFar());
		raf.writeDouble(r.getGPA());
		raf.writeBoolean(r.isStudentType());
	}
	
	
	//read
	public Record read(int index) throws IOException
	{
		if((index < 0) || (index >= size()))
			return null ;
		
		setPointer(index);
			int StudentIdentificationNumber = this.raf.readInt();
			String FirstName = this.raf.readUTF();
			String LastName = this.raf.readUTF();
			int CreditsSoFar = this.raf.readInt();
			double GPA = this.raf.readDouble();
			boolean studentType = this.raf.readBoolean();

		return new Record(StudentIdentificationNumber,FirstName,LastName,CreditsSoFar,GPA,studentType);
	}
	
	//setPointer & size
	public void setPointer(int index)
	{
		try
		{
			int recordSize = 2;
			if(index == 0)
			{
				this.raf.seek(0);
			}
			else if((index > 0) && (index < size()))
			{
				this.raf.seek(index * recordSize);
			}
			else
			{
				this.raf.seek(this.raf.length());
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public int size()
	{
		int recordSize = 2;
		int size = 0;
		try
		{
			size = (int)(this.raf.length()/recordSize);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		
		return size;
	}
	
	//print
	public void print() throws IOException 
	{
		int recordCount = this.size();
		Record r = null;
		for(int i = 0; i < recordCount; i++)
		{
			r = read(i);
			System.out.println(r);
		}
	}
	//add
	public void add(int StudentIdentificationNumber,String FirstName,
			String LastName,int CreditsSoFar,double GPA,boolean studentType)
	{
		try {
			this.raf.writeInt(StudentIdentificationNumber);
			this.raf.writeUTF(FirstName);
			this.raf.writeUTF(LastName);
			this.raf.writeInt(CreditsSoFar);
			this.raf.writeDouble(GPA);
			this.raf.writeBoolean(studentType);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Record newrd = new Record(StudentIdentificationNumber,FirstName,LastName,
				CreditsSoFar,GPA,studentType);
		write(newrd);
	}
	//find
	public boolean find(String strName)
	{
		if(this.file.exists())
		{
			
		}
		return false;
	}
	//edit
	public void edit()
	{
		//this.file.seek();
	}
	//delete
	public void delete(Record d) throws IOException
	{
		for(int i = 0; i < this.size(); i++)
		{
			Record r = read(i);
			if(r.equals(d))
			{
				r = read(this.size());
				write(r,i);
				//delete(d);
				
				raf.setLength(raf.length() - Record.Size);
			}
		}
	}
	
	
	//delete file
	public boolean deleteF(String strPath,String strName)
	{
		this.file = new File(this.strPath + this.strName);
		if(this.file.exists())
		{
			 this.file.delete();
			 return true;
		}
		return false;
	}
	//deleteAll
	public void deleteAll(String strPath, String strName) throws IOException
	{
		raf.setLength(0);
		System.out.println("It is clear");
	}
	//show all
	public void showAll(String strPath, String strName,RandomAccessFile raf) throws IOException 
	{
		int recordSize = 6;
		for(int i =0; i < raf.length()/recordSize; i++)
		{
		System.out.println(this.raf.readInt());
		System.out.println(this.raf.readUTF());
		System.out.println(this.raf.readUTF());
		System.out.println(this.raf.readInt());;
		System.out.println(this.raf.readDouble());
		System.out.println(this.raf.readBoolean());
		}
	}
	//count
	public void count() throws IOException
	{
		System.out.println("Record:" + this.size());
	}

	
}
