package Tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.junit.Test;

import Controller.RecordManager;
import Model.Record;
import Utility.ScannerUtility;

public class RecordManagerTest {


	@Test
	public void testAdd() throws IOException 
	{
		String filePath = ScannerUtility.getString("Enter File Path");
		String fileName = ScannerUtility.getString("Enter File Name");
		
		RecordManager mF = new RecordManager(filePath, fileName,"rw");
		
		boolean success = mF.open(filePath, fileName, true);
		assertTrue(success);
		int length = mF.size();
		Record r = new Record(3,"bob","Will",12,59,true);
	}

	@Test
	public void testDeleteF() throws FileNotFoundException 
	{
		String strName = "testDeleteF.txt", strPath = "c:/temp/";
		
		File file = new File(strPath + strName);
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		Record r = new Record(1, "a", "b", 2, 3.5, true);
		r.write(raf,r);
		
	}

	@Test
	public void testShowAll() throws IOException 
	{

		String strName = "testShowAll.txt", strPath = "c:/temp/";

		File file = new File(strPath + strName);
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		//Record r = new Record(1, "weir", "bug", 45, 55.0, true);
		Record r1 = new Record(1, "Anna", "Will", 5, 95.0, true);
		r1.write(raf,r1);
		
		assertEquals(Record.Size, raf.length());
		
		
		raf.seek(0); //reset read pointer to file start
		
		
		Record rIn = Record.read(raf);
		assertNotNull(rIn);
		
		
		assertEquals(rIn.getStudentIdentificationNumber(), 1);
		assertEquals(rIn.getFirstName(), 2);
		assertEquals(rIn.getLastName(), 3);
		assertEquals(rIn.getCreditsSoFar(), 4);
		assertEquals(rIn.getGPA(), 5.0);
		assertEquals(rIn.isStudentType(), 6);
		
		raf.close();
	}

	@Test
	public void testCount() 
	{
	}

}
