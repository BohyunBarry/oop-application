package Tests;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Model.Record;

public class RecordTest {




	@Test
	public void testRead() throws IOException 
	{
		String strName = "testRead.txt", strPath = "c:/temp/";
		File file = new File(strPath + strName);
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		Record r = new Record(1, "a", "b", 2, 3.5, true);
		r.write(raf,r);
		
		assertEquals(Record.Size, raf.length());
		
		
		raf.seek(0); //reset read pointer to file start
		
		
		Record rIn = Record.read(raf);
		assertNotNull(rIn);
		
		assertEquals(rIn.getStudentIdentificationNumber(), 1);
		assertEquals(rIn.getFirstName(), 2);
		assertEquals(rIn.getLastName(), 3);
		assertEquals(rIn.getCreditsSoFar(), 4);
		assertEquals(rIn.getGPA(), (double)5);
		assertEquals(rIn.isStudentType(), 6);
		//repeat for each field
		
		raf.close();
		
	}

	@Test
	public void testWrite() throws IOException {

		String strName = "testWrite.txt", strPath = "c:/temp/";
		File file = new File(strPath + strName);
		RandomAccessFile raf = new RandomAccessFile(file, "rw");
		Record r = new Record(1, "a", "b", 2, 3.5, true);
		boolean success = r.write(raf,r);
		assertTrue(success);
		assertEquals(Record.Size, raf.length());
		
		
	}

}
