package Model;
import java.io.IOException;
import java.io.RandomAccessFile;
import Utility.StringUtility;

public class Record 
{
	public static final int Size = 2 * (2 + StringUtility.StandardPadLength) + 17;
	
	private int StudentIdentificationNumber; //4
	private String FirstName, LastName;   //(2 + 16) x 2 = 36
	private int CreditsSoFar; //4
	private double GPA; //8
	private boolean studentType; //1


	
	public Record(int studentIdentificationNumber, String firstName, String lastName, int creditsSoFar, double gPA,
			boolean studentType) {
		super();
		StudentIdentificationNumber = studentIdentificationNumber;
		FirstName = firstName;
		LastName = lastName;
		CreditsSoFar = creditsSoFar;
		GPA = gPA;
		this.studentType = studentType;
	}
	public int getStudentIdentificationNumber() {
		return StudentIdentificationNumber;
	}
	public void setStudentIdentificationNumber(int studentIdentificationNumber) {
		StudentIdentificationNumber = studentIdentificationNumber;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getCreditsSoFar() {
		return CreditsSoFar;
	}
	public void setCreditsSoFar(int creditsSoFar) {
		CreditsSoFar = creditsSoFar;
	}
	public double getGPA() {
		return GPA;
	}
	public void setGPA(double gPA) {
		GPA = gPA;
	}
	public boolean isStudentType() {
		return studentType;
	}
	public void setStudentType(boolean studentType) {
		this.studentType = studentType;
	}
	@Override
	public String toString() {
		return "Record [StudentIdentificationNumber=" + StudentIdentificationNumber + ", FirstName=" + FirstName
				+ ", LastName=" + LastName + ", CreditsSoFar=" + CreditsSoFar + ", GPA=" + GPA + ", studentType="
				+ studentType + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + CreditsSoFar;
		result = prime * result + ((FirstName == null) ? 0 : FirstName.hashCode());
		long temp;
		temp = Double.doubleToLongBits(GPA);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((LastName == null) ? 0 : LastName.hashCode());
		result = prime * result + StudentIdentificationNumber;
		result = prime * result + (studentType ? 1231 : 1237);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (CreditsSoFar != other.CreditsSoFar)
			return false;
		if (FirstName == null) {
			if (other.FirstName != null)
				return false;
		} else if (!FirstName.equals(other.FirstName))
			return false;
		if (Double.doubleToLongBits(GPA) != Double.doubleToLongBits(other.GPA))
			return false;
		if (LastName == null) {
			if (other.LastName != null)
				return false;
		} else if (!LastName.equals(other.LastName))
			return false;
		if (StudentIdentificationNumber != other.StudentIdentificationNumber)
			return false;
		if (studentType != other.studentType)
			return false;
		return true;
	}
	
	
	public static Record read(RandomAccessFile raf)
	{
		try
		{
			String FirstName = StringUtility.unpad(raf.readUTF(),
					StringUtility.PadString);
			String LastName = StringUtility.unpad(raf.readUTF(),
					StringUtility.PadString);
			
			int StudentIdentificationNumber = raf.readInt();
			int CreditsSoFar = raf.readInt();
			double GPA = raf.readDouble();
			boolean studentType = raf.readBoolean();
			
			return new Record(StudentIdentificationNumber,FirstName,LastName,CreditsSoFar,GPA,studentType);
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean write(RandomAccessFile raf, Record r)
	{
		try
		{
			raf.writeInt(r.getStudentIdentificationNumber());
			raf.writeUTF(r.getFirstName());
			raf.writeUTF(r.getLastName());
			raf.writeInt(r.getCreditsSoFar());
			raf.writeDouble(r.getGPA());
			raf.writeBoolean(r.isStudentType());
			//raf.writeUTF(StringUtility.pad(
					//this.getFirstName(),
					//StringUtility.StandardPadLength,
					//StringUtility.PadString));
			///raf.writeUTF(StringUtility.pad(
					//this.getLastName(),
					//StringUtility.StandardPadLength,
					//StringUtility.PadString));
			//raf.writeInt(this.StudentIdentificationNumber);
			//raf.writeInt(this.CreditsSoFar);
			//raf.writeDouble(this.GPA);
			//raf.writeBoolean(this.studentType);
			
			return true;
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
		return false;
	}
	
}
