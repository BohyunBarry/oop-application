package View;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import Model.Record;
import Model.Song;
import Controller.RecordManager;

public class MainApp 
{
	private MenuOptions mainMenu,regMenu,delMenu;
	private RecordManager rManager;

	static Scanner kb = new Scanner(System.in);
	
	public static void main(String[] args) throws Exception
	{
		MainApp theApp = new MainApp();
		theApp.start();
	}

	private void start() throws Exception
	{
		rManager = this.rManager;
		System.out.println("University");
		initialiseMenus();
		showMainMenu();
		
		String strName = "demoFileWriteReader.txt";
		String strPath = "c:/temp/";
		boolean bAppend = false;
		demoFileWriter(strPath, strName, bAppend);
		demoFileReader(strPath, strName);
		
		//Buffered writer & reader
		strName = "demoBufferedWriterReader.txt";
		strPath = "c:/temp/";
		bAppend =false;
		int bufferSize = 10;
		demoBufferedWriter(strPath, strName, bAppend, bufferSize);
		
		//to do
		demoBufferedReader(strPath, strName, bufferSize);
		
		//PrintWriter
		strName = "demoPrintWriter.csv";
		strPath = "c:/temp/";
		bAppend = false;
		bufferSize = 1024;
		//demoPrintWriter(strPath, strName, bAppend, bufferSize);
		
		//XML
		List<Song> list = new ArrayList<Song>();
		list.add(new Song("name1", "artist1", (float)3.25));
		list.add(new Song("name2", "artist2", (float)2.85));
		list.add(new Song("name3", "artist3", (float)2.6));
		
		//serialize song data from a list to XML
		strName = "demoListToXML.xml";
		strPath = "c:/temp/";
		demoListToXML(strPath, strName, list);
		
		//HTML
		strName = "demoListToHTML.html";
		strPath = "c:/temp/";
		demoListToHTML(strPath, strName, list);
		
		System.out.println("done!");
	}
	
	private void demoListToXML(String strPath,
			String strName, List<Song> list) throws FileNotFoundException
	{
		PrintWriter pWriter = new PrintWriter(strPath + strName);
		
		pWriter.println("<xml>");
		pWriter.println("<catalogue>");
		for(Song s : list)
		{
			pWriter.println("<song>");
			pWriter.println(s.toXML());
			pWriter.println("<song>");
		}
		pWriter.println("</catalogue>");
		pWriter.println("</xml>");
		pWriter.close();
	}
	
	private void demoListToHTML(String strPath,
			String strName, List<Song> list) throws FileNotFoundException
	{
		PrintWriter pWriter = new PrintWriter(strPath + strName);
		
		pWriter.println("<html>");
		pWriter.println("<head><title>Songs</title></head>");
		pWriter.println("<body>");
		pWriter.println("<table border='1'>");
		pWriter.println("<tr><td>Name</td><td>Artist</td><td>Running Time</td></tr>");
		
		for(Song s : list)
		{
			pWriter.println("<tr>");
			pWriter.println(s.toHTMLTableData());
			pWriter.println("</tr>");
		}
		
		pWriter.println("</table>");
		pWriter.println("</body>");
		pWriter.println("</html>");
		pWriter.close();
	}
	
	private void demoPrintWriter(String strPath,
			String strName, boolean bAppend, int bufferSize) throws IOException
	{
		File file = new File(strPath, strName);
		FileWriter fWriter = new FileWriter(file, bAppend);
		BufferedWriter bWriter = new BufferedWriter(fWriter, bufferSize);
		PrintWriter pWriter = new PrintWriter(bWriter);
		
		pWriter.println("StudentIdentificationNumber, FirstName, LastName, CreditsSoFar,GPA,StudentType");
		pWriter.println("1,John,Smith, 45, 56,true");
		pWriter.println("2,Jane,Doe, 78, 89,false");
		pWriter.println("3,Lol,Will, 65, 105,false");
		pWriter.println("4,Mar,Ck, 8, 91,true");
		
		pWriter.flush();
		pWriter.close();
	}
	
	private void demoBufferedReader(String strPath,
			String strName, int bufferSize)
	{
		
	}
	
	private void demoBufferedWriter(String strPath,
			String strName, boolean bAppend, int bufferSize) throws IOException
	{
		File file = new File(strPath + strName);
		FileWriter fWriter = new FileWriter(file, bAppend);
		BufferedWriter bWriter = new BufferedWriter(fWriter, bufferSize);
		
		bWriter.write("Niall");
		bWriter.write("John");
		bWriter.write("May");
		
		bWriter.flush();
		bWriter.close();
	}
	
	private void demoFileReader(String strPath, String strName) throws IOException
	{
		File file = new File(strPath, strName);
		FileReader fReader = new FileReader(file);
		
		char[] cbuf = new char[1024];
		fReader.read(cbuf);
		String s = String.valueOf(cbuf);
		System.out.println(s);
		fReader.close();
	}
	
	private void demoFileWriter(String strPath, String strName, boolean bAppend) throws IOException
	{
		File file = new File(strPath, strName);
		FileWriter fWriter = new FileWriter(file, bAppend);
		
		String s = "QWERTY";
		fWriter.write(s,0,s.length());
		fWriter.flush();
		fWriter.close();
	}


	//menu
	private void showMainMenu() throws Exception 
	{
          int choice = 0;
		
		do
		{
			choice = this.mainMenu.showMenuGetChoice("What do you want to do? ");
			
			
			if(choice == 1)
				showRegMenu();
			else if(choice == 2)
				showOpentMenu();
			else if(choice == 3)
				showAddMenu();
			else if(choice == 4)
				showDelMenu();
			else if(choice == 5)
				showShowMenu();
			else if(choice == 6)
				showCountMenu();
			else
				showQuitMenu();
			
		}while(choice != this.mainMenu.getIndexOfExitOption());
		
		System.out.println("Goodbye...");
	}
	

	private void showShowMenu() throws IOException  
	{
		String strName = "derv2.txt";
		String strPath = "c:/temp/";
		RandomAccessFile raf = new RandomAccessFile(strPath+strName,"rw");
		rManager.showAll(strPath,strName, raf);
	}

	private void showAddMenu() throws IOException 
	{
		String strName = "demoPrintWriter.csv";
		String strPath = "c:/temp/";
		boolean bAppend = false;
		int bufferSize = 1024;
		demoPrintWriter(strPath, strName, bAppend, bufferSize);
		System.out.println("done!");
	}

	private void showQuitMenu() 
	{
		System.out.println("Good Bye!!!!");
	}

	private void showCountMenu() throws IOException 
	{
		rManager.count();
	}

	private void showDelMenu() throws Exception 
	{
		System.out.println("Please enter file path...");
		String strPath = kb.next();
		System.out.println("Please enter file name...");
		String strName = kb.next();
		
		System.out.println("Enter Student Identification Number...");
		int StudentIdentificationNumber = kb.nextInt();
		System.out.println("Enter First Name...");
		String FirstName = kb.next();
		System.out.println("Enter Last Name...");
		String LastName = kb.next();
		System.out.println("Enter Credits So Far...");
		int CreditsSoFar = kb.nextInt();
		System.out.println("Enter GPA...");
		double GPA = kb.nextDouble();
		System.out.println("Enter studentType...");
		boolean studentType = kb.nextBoolean();
		Record rd = new Record(StudentIdentificationNumber,FirstName,LastName,CreditsSoFar,GPA,studentType);
		//rManager.delete(rd);
		System.out.println("Done!" + rd);
		
		int choice = 0;
		
		do
		{
			choice = this.delMenu.showMenuGetChoice("Do you want to delete?");
			if(choice == 1)
			{
				System.out.println("Please enter file path...");
				strPath = kb.next();
				System.out.println("Please enter file name...");
				strName = kb.next();
				System.out.println(rManager.deleteF(strPath,strName));
			}
			else if(choice == 2)
			{		
				System.out.println("Please enter file path...");
				strPath = kb.next();
				System.out.println("Please enter file name...");
				strName = kb.next();
				rManager.deleteAll(strPath,strName);
			}
			else
			{
				
				showMainMenu();
			}
		}while(choice != this.regMenu.getIndexOfExitOption());
	}

	private void showOpentMenu() throws Exception 
	{
		System.out.println("Please enter a file name...");
		String strName = kb.next();
		System.out.println("Please enter path to file...");
		String strPath = kb.next();
		
		String strPermission = "rw";
		
		rManager = new RecordManager(strPath, strName,strPermission);
		boolean bAppend = false;
		System.out.println(rManager.open(strPath, strName,bAppend));
		
		int choice = 0;
			
		do
		{
			choice = this.regMenu.showMenuGetChoice("Do you want to add?");
			if(choice == 1)
			{
				System.out.println("Enter Student Identification Number...");
				int StudentIdentificationNumber = kb.nextInt();
				System.out.println("Enter First Name...");
				String FirstName = kb.next();
				System.out.println("Enter Last Name...");
				String LastName = kb.next();
				System.out.println("Enter Credits So Far...");
				int CreditsSoFar = kb.nextInt();
				System.out.println("Enter GPA...");
				double GPA = kb.nextDouble();
				System.out.println("Enter studentType...");
				boolean studentType = kb.nextBoolean();
				rManager.add(StudentIdentificationNumber,FirstName,LastName,CreditsSoFar,GPA,studentType);
			}
			else
			{
				showMainMenu();
			}
		}while(choice != this.regMenu.getIndexOfExitOption());
	}

	private void showRegMenu() throws IOException 
	{
		System.out.println("Please enter a file name...");
		String strName = kb.next();
		System.out.println("Please enter path to file...");
		String strPath = kb.next();
		String strPermission = "rw";
		
		rManager = new RecordManager(strPath, strName,strPermission);
		System.out.println(rManager.create(strPath,strName));
	}

	private void initialiseMenus() 
	{
		this.mainMenu = new MenuOptions("Main Menu", 7);
		this.mainMenu.add("Create");
		this.mainMenu.add("Open");
		this.mainMenu.add("Add");
		this.mainMenu.add("Delete");
		this.mainMenu.add("Show All");
		this.mainMenu.add("Count");
		this.mainMenu.add("Quit");
		
		
		this.regMenu = new MenuOptions("Add Menu", 1);
		this.regMenu.add("Add:");
		
		this.delMenu = new MenuOptions("Delete Menu", 2);
		this.delMenu.add("Delete File");
		this.delMenu.add("Delete All");
	}
	
	
}
