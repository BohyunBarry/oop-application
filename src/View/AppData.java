package View;

/**
 * Stores static constants used to perform regex based validation of
 * drive paths, file names, file permissions etc - see RecordManager::RecordManager()
 * @author NMCG
 * @version 1.0
 */
public class AppData 
{
	public static final String DefaultDriverLetter = System.getenv("SystemDrive");
	public static final String DefaultFilePath = DefaultDriverLetter + "/temp";
	public static final String RegexFilePath = "([a-zA-Z]:(/|\\\\)){1}([a-zA-Z0-9_-]{1,}(/|\\\\)){0,}";
	public static final String RegexFilePermission = "(r|rw|rws|rwd)";
	public static final String DefaultFilePermission = "rw";
	public static final String RegexFileName = "[a-zA-Z]{1,}[a-zA-Z0-9]{0,}\\.[a-zA-Z]{1,4}";
	public static final String DefaultFileName = "myData.txt";
}
